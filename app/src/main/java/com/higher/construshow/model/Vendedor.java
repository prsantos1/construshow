package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Vendedor implements Serializable {

    @SerializedName("idpess")
    private int id;
    @SerializedName("idpessoa")
    private int idPessoa;
    @SerializedName("fantasia")
    private String nome;
    private String email;
    @SerializedName("userid")
    private String userId;
    private double saldoFlex;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getSaldoFlex() {
        return saldoFlex;
    }

    public void setSaldoFlex(double saldoFlex) {
        this.saldoFlex = saldoFlex;
    }
}
