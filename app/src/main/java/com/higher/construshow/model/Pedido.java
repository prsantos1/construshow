package com.higher.construshow.model;

import java.io.Serializable;

public class Pedido implements Serializable {

    private int id;
    private int idEstab;
    private int idCliente;
    private int idVendedor;
    private int idEndereco;
    private String data;
    private double qtdVolume;
    private String observacao;
    private int idCarrinho;
    private int qtdProdutos;
    private double valorLiquido;
    private double valorTotal;
    private int enviado;
    private String formaPagamento;
    private int idTabelaPreco;
    private int idParcelamento;
    private double valorFlex;
    private int idDepartamento;

    private String nomeTabPreco;
    private String nomeCliente;
    private String endereco;

    public double getValorLiquido() {
        return valorLiquido;
    }

    public void setValorLiquido(double valorLiquido) {
        this.valorLiquido = valorLiquido;
    }

    public String getNomeTabPreco() {
        return nomeTabPreco;
    }

    public void setNomeTabPreco(String nomeTabPreco) {
        this.nomeTabPreco = nomeTabPreco;
    }

    public int getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public double getValorFlex() {
        return valorFlex;
    }

    public void setValorFlex(double valorFlex) {
        this.valorFlex = valorFlex;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public int getIdTabelaPreco() {
        return idTabelaPreco;
    }

    public void setIdTabelaPreco(int idTabelaPreco) {
        this.idTabelaPreco = idTabelaPreco;
    }

    public int getIdParcelamento() {
        return idParcelamento;
    }

    public void setIdParcelamento(int idParcelamento) {
        this.idParcelamento = idParcelamento;
    }

    public int getEnviado() {
        return enviado;
    }

    public void setEnviado(int enviado) {
        this.enviado = enviado;
    }

    public int getQtdProdutos() {
        return qtdProdutos;
    }

    public void setQtdProdutos(int qtdProdutos) {
        this.qtdProdutos = qtdProdutos;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(int idEstab) {
        this.idEstab = idEstab;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public int getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getQtdVolume() {
        return qtdVolume;
    }

    public void setQtdVolume(double qtdVolume) {
        this.qtdVolume = qtdVolume;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getIdCarrinho() {
        return idCarrinho;
    }

    public void setIdCarrinho(int idCarrinho) {
        this.idCarrinho = idCarrinho;
    }
}
