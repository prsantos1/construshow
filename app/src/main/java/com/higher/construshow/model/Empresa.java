package com.higher.construshow.model;

import java.io.Serializable;

public class Empresa implements Serializable {

    private int id;
    private String nome;
    private String ultimaSinc;
    private String ultimaSincImg;
    private String ultimaSinc2;

    public Empresa(){};

    public Empresa(int id, String nome){
        this.id = id;
        this.nome = nome;
    }

    public String getUltimaSinc2() {
        return ultimaSinc2;
    }

    public void setUltimaSinc2(String ultimaSinc2) {
        this.ultimaSinc2 = ultimaSinc2;
    }

    public String getUltimaSinc() {
        return ultimaSinc;
    }

    public void setUltimaSinc(String ultimaSinc) {
        this.ultimaSinc = ultimaSinc;
    }

    public String getUltimaSincImg() {
        return ultimaSincImg;
    }

    public void setUltimaSincImg(String ultimaSincImg) {
        this.ultimaSincImg = ultimaSincImg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
