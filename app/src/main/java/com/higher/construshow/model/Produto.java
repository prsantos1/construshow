package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Produto implements Serializable {

    @SerializedName("iditem")
    private String id;
    @SerializedName("estab")
    private String idEstab;
    private String descricao;
    private String unidade;
    private double preco;
    @SerializedName("saldo")
    private double estoque;
    @SerializedName("idsecao")
    private int idSecao;
    @SerializedName("idgrupoitem")
    private int idGrupo;
    @SerializedName("iddepto")
    private int idDepartamento;
    @SerializedName("idmarca")
    private int idMarca;
    @SerializedName("marca")
    private String marca;

    @SerializedName("preco_imposto")
    private double porcentagemImposto;
    @SerializedName("peso_unidade")
    private double pesoUnidade;

    public double getPorcentagemImposto() {
        return porcentagemImposto;
    }

    public void setPorcentagemImposto(double porcentagemImposto) {
        this.porcentagemImposto = porcentagemImposto;
    }

    public double getPesoUnidade() {
        return pesoUnidade;
    }

    public void setPesoUnidade(double pesoUnidade) {
        this.pesoUnidade = pesoUnidade;
    }

    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    private double precoLiquido;

    public int getIdSecao() {
        return idSecao;
    }

    public void setIdSecao(int idSecao) {
        this.idSecao = idSecao;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public int getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public double getPrecoLiquido() {
        return precoLiquido;
    }

    public void setPrecoLiquido(double precoLiquido) {
        this.precoLiquido = precoLiquido;
    }

    public String getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(String idEstab) {
        this.idEstab = idEstab;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getEstoque() {
        return estoque;
    }

    public void setEstoque(double estoque) {
        this.estoque = estoque;
    }
}
