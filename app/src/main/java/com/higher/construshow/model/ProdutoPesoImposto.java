package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProdutoPesoImposto implements Serializable {
    private int id;
    @SerializedName("preco_imposto")
    private double porcentagemImposto;
    @SerializedName("peso_unidade")
    private double pesoUnidade;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPorcentagemImposto() {
        return porcentagemImposto;
    }

    public void setPorcentagemImposto(double porcentagemImposto) {
        this.porcentagemImposto = porcentagemImposto;
    }

    public double getPesoUnidade() {
        return pesoUnidade;
    }

    public void setPesoUnidade(double pesoUnidade) {
        this.pesoUnidade = pesoUnidade;
    }
}
