package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TabelaPreco implements Serializable {

    @SerializedName("idtabprvda")
    private int id;
    @SerializedName("id_estab")
    private int idEstab;
    private String descricao;
    private String tipo;
    @SerializedName("percpadrao")
    private double percentualPadrao;
    @SerializedName("permitedesconto")
    private String permiteDesconto;
    @SerializedName("priorizapromocao")
    private String priorizaPromocao;
    @SerializedName("aplicaem")
    private String aplicaEm;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(int idEstab) {
        this.idEstab = idEstab;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getPercentualPadrao() {
        return percentualPadrao;
    }

    public void setPercentualPadrao(double percentualPadrao) {
        this.percentualPadrao = percentualPadrao;
    }

    public String getPermiteDesconto() {
        return permiteDesconto;
    }

    public void setPermiteDesconto(String permiteDesconto) {
        this.permiteDesconto = permiteDesconto;
    }

    public String getPriorizaPromocao() {
        return priorizaPromocao;
    }

    public void setPriorizaPromocao(String priorizaPromocao) {
        this.priorizaPromocao = priorizaPromocao;
    }

    public String getAplicaEm() {
        return aplicaEm;
    }

    public void setAplicaEm(String aplicaEm) {
        this.aplicaEm = aplicaEm;
    }
}
