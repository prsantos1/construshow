package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConceitoCliente implements Serializable {

    @SerializedName("idconceito")
    private int id;
    @SerializedName("empresa")
    private int idEstab;
    private String descricao;
    @SerializedName("formacart")
    private String formaCart;
    @SerializedName("formadup")
    private String formaDup;
    @SerializedName("formache")
    private String formaCheque;
    @SerializedName("permitepromocao")
    private String permitePromocao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(int idEstab) {
        this.idEstab = idEstab;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getFormaCart() {
        return formaCart;
    }

    public void setFormaCart(String formaCart) {
        this.formaCart = formaCart;
    }

    public String getFormaDup() {
        return formaDup;
    }

    public void setFormaDup(String formaDup) {
        this.formaDup = formaDup;
    }

    public String getFormaCheque() {
        return formaCheque;
    }

    public void setFormaCheque(String formaCheque) {
        this.formaCheque = formaCheque;
    }

    public String getPermitePromocao() {
        return permitePromocao;
    }

    public void setPermitePromocao(String permitePromocao) {
        this.permitePromocao = permitePromocao;
    }
}
