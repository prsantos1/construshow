package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Cliente implements Serializable {

    @SerializedName("idpessoa")
    private String id;
    private String nome;
    private String fantasia;
    @SerializedName("estab")
    private String idEstab;
    private String cnpjf;
    private String email;
    @SerializedName("idtipopessoa")
    private String idTipo;
    private String uf;
    private String ativo;
    @SerializedName("idconceito")
    private String idConceito;
    @SerializedName("desc_conceito")
    private String descConceito;
    @SerializedName("idregiao")
    private String idRegiao;

    private String endereço;
    private String cidade;

    public String getIdConceito() {
        return idConceito;
    }

    public void setIdConceito(String idConceito) {
        this.idConceito = idConceito;
    }

    public String getDescConceito() {
        return descConceito;
    }

    public void setDescConceito(String descConceito) {
        this.descConceito = descConceito;
    }

    public String getIdRegiao() {
        return idRegiao;
    }

    public void setIdRegiao(String idRegiao) {
        this.idRegiao = idRegiao;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(String idEstab) {
        this.idEstab = idEstab;
    }

    public String getCnpjf() {
        return cnpjf;
    }

    public void setCnpjf(String cnpjf) {
        this.cnpjf = cnpjf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }
}
