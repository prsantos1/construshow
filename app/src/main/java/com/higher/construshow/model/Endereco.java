package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Endereco implements Serializable {

    @SerializedName("estab")
    private String idEstab;
    @SerializedName("idpess")
    private String idCliente;
    @SerializedName("idend")
    private String idEndereco;
    @SerializedName("tipoend")
    private String tipo;
    private String endereco;
    private String complemento;
    @SerializedName("numend")
    private String numero;
    private String bairro;
    @SerializedName("cidade")
    private String idCidade;
    @SerializedName("nomecidade")
    private String cidade;
    private String cep;
    private String telefone;

    private String uf;

    public String toString(){
        String retorno = "";
        retorno = tipo.equals("P") ? "Principal" : "Cobrança";
        retorno = retorno + ": " + endereco;
        retorno = retorno + (numero == null || numero.equals("") ? "" : ", " + numero);
        retorno = retorno + (bairro == null ? "" : " - " + bairro);

        return retorno;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(String idEstab) {
        this.idEstab = idEstab;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(String idEndereco) {
        this.idEndereco = idEndereco;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(String idCidade) {
        this.idCidade = idCidade;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
