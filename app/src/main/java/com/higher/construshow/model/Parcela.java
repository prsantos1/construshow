package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Parcela implements Serializable {

    @SerializedName("idparcela")
    private int id;
    private String descricao;
    @SerializedName("nroparcelas")
    private int numeroParcelas;
    private int carencia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getNumeroParcelas() {
        return numeroParcelas;
    }

    public void setNumeroParcelas(int numeroParcelas) {
        this.numeroParcelas = numeroParcelas;
    }

    public int getCarencia() {
        return carencia;
    }

    public void setCarencia(int carencia) {
        this.carencia = carencia;
    }
}
