package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.PedidoLancto;
import com.higher.construshow.util.Repositorio;

import java.util.List;

public class PedidoLanctoAdapter extends BaseQuickAdapter<PedidoLancto, BaseViewHolder> {

    public PedidoLanctoAdapter(@Nullable List<PedidoLancto> data) {
        super(R.layout.card_lanctos, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, PedidoLancto item) {
        helper.setText(R.id.txvProduto, "#"+item.getSequencia() + " - " + item.getNomeProduto())
                .setText(R.id.txvPreco, "R$ " + Repositorio.formataNumero(item.getPrecoVisualizacao()))
                .setText(R.id.txvQuantidade, Repositorio.formataNumero(item.getQuantidade()))
                .setText(R.id.txvDesconto, Repositorio.formataNumero(item.getDescItem()) + "%")
                .setText(R.id.txvValorDesconto, "R$ " + Repositorio.formataNumero(item.getValorDescontoVisualizacao()))
                .setText(R.id.txvValorTotal,"R$ " + Repositorio.formataNumero(item.getValorTotal()));
    }
}
