package com.higher.construshow.adapter;

import android.content.res.ColorStateList;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.transition.Visibility;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Pedido;
import com.higher.construshow.util.Repositorio;

import java.util.List;

public class PedidoAdapter extends BaseQuickAdapter<Pedido, BaseViewHolder> {

    public PedidoAdapter(@Nullable List<Pedido> data) {
        super(R.layout.card_pedidos, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Pedido item) {
        helper.setText(R.id.txvCliente, "#"+item.getId() + " - " + item.getNomeCliente())
                .setText(R.id.txvTabelaPreco, item.getNomeTabPreco() + " - " + item.getFormaPagamento())
                .setText(R.id.txvDataPedido, Repositorio.dataBancoParaTela(item.getData()))
                .setText(R.id.txvValorPedido, "R$ " + Repositorio.formataNumero(item.getValorTotal()));

        String _auxQtdProdutos = "Nenhum produto no pedido";
        if(item.getQtdProdutos() == 1){
            _auxQtdProdutos = "1 produto no pedido";
        }else if(item.getQtdProdutos() > 1){
            _auxQtdProdutos = item.getQtdProdutos() + " produtos no pedido";
        }

        helper.setText(R.id.txvQtdItens, _auxQtdProdutos);

        if(item.getIdDepartamento() == 1){
            helper.setVisible(R.id.chipMateriais, true);
            helper.setGone(R.id.chipFerragens, false);
        }else{
            helper.setVisible(R.id.chipFerragens, true);
            helper.setGone(R.id.chipMateriais, false);
        }

        if(item.getEnviado() == 1){
            helper.setVisible(R.id.chipEnviado, true);
            helper.setGone(R.id.chipNaoEnviado, false);
        }else{
            helper.setVisible(R.id.chipNaoEnviado, true);
            helper.setGone(R.id.chipEnviado, false);
        }
    }
}
