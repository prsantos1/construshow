package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Cidade;
import com.higher.construshow.model.Cliente;

import java.util.ArrayList;
import java.util.List;

public class ClienteDao {

    private String TABLE_NAME = "cliente";
    private SQLiteDatabase le, escreve;
    private Database db;

    public ClienteDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(Cliente model) {
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("nome", model.getNome());
        cv.put("fantasia", model.getFantasia());
        cv.put("id_estab", model.getIdEstab());
        cv.put("cnpjf", model.getCnpjf());
        cv.put("email", model.getEmail());
        cv.put("id_tipo", model.getIdTipo());
        cv.put("uf", model.getUf());
        cv.put("ativo", model.getAtivo());
        cv.put("id_conceito", model.getIdConceito());
        cv.put("descricao_conceito", model.getDescConceito());
        cv.put("id_regiao", model.getIdRegiao());

        long id = escreve.insert(TABLE_NAME, null, cv);

        return id;
    }

    public long atualiza(Cliente model) {
        ContentValues cv = new ContentValues();

        cv.put("nome", model.getNome());
        cv.put("fantasia", model.getFantasia());
        cv.put("cnpjf", model.getCnpjf());
        cv.put("email", model.getEmail());
        cv.put("id_tipo", model.getIdTipo());
        cv.put("uf", model.getUf());
        cv.put("ativo", model.getAtivo());
        cv.put("id_conceito", model.getIdConceito());
        cv.put("descricao_conceito", model.getDescConceito());
        cv.put("id_regiao", model.getIdRegiao());

        long id = escreve.update(TABLE_NAME, cv, "id = ? AND id_estab = ?", new String[]{model.getId(), model.getIdEstab()});

        return id;
    }

    public long insertOuUpdate(Cliente model) {

        //Verifica se o cliente já existe
        Cursor c = le.rawQuery("SELECT id FROM cliente WHERE id = ? AND id_estab = ?", new String[]{model.getId(), model.getIdEstab()});
        c.moveToFirst();

        long id = 0;

        if (c != null) {
            if (c.getCount() > 0) {
                //Já existe
                id = atualiza(model);
            } else {
                //Não existe
                id = insere(model);
            }
        }
        c.close();

        return id;
    }

    public Cliente retorna(int id) {
        Cliente cliente = new Cliente();

        String sql =
                "SELECT " +
                        "cl.id, " +
                        "cl.nome," +
                        "cl.fantasia," +
                        "cl.id_estab," +
                        "cl.cnpjf," +
                        "cl.email," +
                        "cl.id_tipo," +
                        "cl.uf," +
                        "cl.ativo, " +
                        "en.endereco," +
                        "COALESCE(en.numero, ''), " +
                        "COALESCE(en.bairro, ''), " +
                        "en.cidade, " +
                        "cl.id_conceito, " +
                        "cl.descricao_conceito, " +
                        "cl.id_regiao " +
                        "FROM " +
                        "cliente cl " +
                        "INNER JOIN endereco en ON (cl.id = en.id_cliente) AND (cl.id_estab = en.id_estab) " +
                        "WHERE " +
                        "en.tipo_end = 'P' AND cl.id = ? " +
                        "ORDER BY " +
                        "cl.nome";

        Cursor c = le.rawQuery(sql, new String[]{String.valueOf(id)});
        c.moveToFirst();

        if (c.getCount() > 0) {

            cliente.setId(c.getString(0));
            cliente.setNome(c.getString(1));
            cliente.setFantasia(c.getString(2));
            cliente.setIdEstab(c.getString(3));
            cliente.setCnpjf(c.getString(4));
            cliente.setEmail(c.getString(5));
            cliente.setIdTipo(c.getString(6));
            cliente.setUf(c.getString(7));
            cliente.setAtivo(c.getString(8));
            cliente.setEndereço(c.getString(9) + ", " + c.getString(10) + " - " + c.getString(11));
            cliente.setCidade(c.getString(12));
            cliente.setIdConceito(c.getString(13));
            cliente.setDescConceito(c.getString(14));
            cliente.setIdRegiao(c.getString(15));

            c.moveToNext();
        }

        c.close();

        return cliente;
    }

    public List<Cliente> retornaTodos(String pesquisa, Cidade cidade) {
        List<Cliente> retorno = new ArrayList<>();

        String sql =
                "SELECT " +
                        "cl.id, " +
                        "cl.nome," +
                        "cl.fantasia," +
                        "cl.id_estab," +
                        "cl.cnpjf," +
                        "cl.email," +
                        "cl.id_tipo," +
                        "cl.uf," +
                        "cl.ativo, " +
                        "en.endereco," +
                        "COALESCE(en.numero, ''), " +
                        "COALESCE(en.bairro, ''), " +
                        "en.cidade, " +
                        "cl.id_conceito, " +
                        "cl.descricao_conceito, " +
                        "cl.id_regiao " +
                        "FROM " +
                        "cliente cl " +
                        "INNER JOIN endereco en ON (cl.id = en.id_cliente) AND (cl.id_estab = en.id_estab) " +
                        "WHERE " +
                        "en.tipo_end = 'P' AND (cl.nome LIKE ? OR cl.fantasia LIKE ?) ";

        if (cidade != null) {
            sql += "AND en.id_cidade = '" + cidade.getId() + "' ";
        }
        sql += "ORDER BY " +
                "cl.nome";

        Cursor c = le.rawQuery(sql, new String[]{pesquisa + "%", pesquisa + "%"});
        c.moveToFirst();


        while (!c.isAfterLast()) {
            Cliente cliente = new Cliente();

            cliente.setId(c.getString(0));
            cliente.setNome(c.getString(1));
            cliente.setFantasia(c.getString(2));
            cliente.setIdEstab(c.getString(3));
            cliente.setCnpjf(c.getString(4));
            cliente.setEmail(c.getString(5));
            cliente.setIdTipo(c.getString(6));
            cliente.setUf(c.getString(7));
            cliente.setAtivo(c.getString(8));
            cliente.setEndereço(c.getString(9) + ", " + c.getString(10) + " - " + c.getString(11));
            cliente.setCidade(c.getString(12));
            cliente.setIdConceito(c.getString(13));
            cliente.setDescConceito(c.getString(14));
            cliente.setIdRegiao(c.getString(15));

            retorno.add(cliente);

            c.moveToNext();
        }
        c.close();

        return retorno;
    }

    public int qtdClientes(){
        int retorno = 0;

        String sql = "SELECT COUNT(id) FROM cliente";
        Cursor c = le.rawQuery(sql, null);
        c.moveToFirst();

        if(c.getCount() > 0){
            retorno = c.getInt(0);
        }
        c.close();

        return retorno;
    }
}
