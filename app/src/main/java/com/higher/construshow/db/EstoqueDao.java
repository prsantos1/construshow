package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Estoque;

public class EstoqueDao {

    private String TABLE_NAME = "estoque";
    private SQLiteDatabase le, escreve;
    private Database db;

    public EstoqueDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    private long insere(Estoque model){
        ContentValues cv = new ContentValues();

        cv.put("id_estab", model.getIdEstab());
        cv.put("id_item", model.getIdItem());
        cv.put("saldo", model.getSaldo());

        return escreve.insert(TABLE_NAME, null, cv);
    }

    private long atualiza(Estoque model){
        ContentValues cv = new ContentValues();

        cv.put("saldo", model.getSaldo());

        return escreve.update(TABLE_NAME, cv, "id_item = ? AND id_estab = ?", new String[]{ String.valueOf(model.getIdItem()), String.valueOf(model.getIdEstab()) });
    }

    public long insertOuUpdate(Estoque model){
        long retorno = 0;

        String sql = "SELECT id_item FROM estoque WHERE id_item = ? AND id_estab = ?";
        Cursor c = le.rawQuery(sql, new String[] { String.valueOf(model.getIdItem()), String.valueOf(model.getIdEstab()) });
        c.moveToFirst();

        if(c.getCount() > 0){
            retorno = atualiza(model);
        }else{
            retorno = insere(model);
        }
        c.close();

        return retorno;
    }
}
