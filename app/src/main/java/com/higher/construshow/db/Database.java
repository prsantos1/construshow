package com.higher.construshow.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

    private static Database mInstance = null;

    public static String DATABASE_NAME = "construshow_db";
    private static final int DATABASE_VERSION = 1;

    public static Database getInstance(Context c){
        if(mInstance == null){
            mInstance = new Database(c.getApplicationContext());
        }

        return mInstance;
    }

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Tabelas.TABLE_CIDADE);
        db.execSQL(Tabelas.TABLE_CLIENTE);
        db.execSQL(Tabelas.TABLE_CONCEITO_CLIENTE);
        db.execSQL(Tabelas.TABLE_DEPARTAMENTO);
        db.execSQL(Tabelas.TABLE_EMPRESA);
        db.execSQL(Tabelas.TABLE_ENDERECO);
        db.execSQL(Tabelas.TABLE_ESTOQUE);
        db.execSQL(Tabelas.TABLE_PARCELAS);
        db.execSQL(Tabelas.TABLE_PEDIDO);
        db.execSQL(Tabelas.TABLE_PEDIDO_LANCTOS);
        db.execSQL(Tabelas.TABLE_PRODUTO);
        db.execSQL(Tabelas.TABLE_TABELA_PRECO);
        db.execSQL(Tabelas.TABLE_TIPO_PESSOA);
        db.execSQL(Tabelas.TABLE_VENDEDOR);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
