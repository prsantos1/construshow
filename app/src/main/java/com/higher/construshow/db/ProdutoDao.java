package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.Nullable;

import com.higher.construshow.model.Produto;
import com.higher.construshow.model.ProdutoPesoImposto;
import com.higher.construshow.model.TabelaPreco;

import java.util.ArrayList;
import java.util.List;

public class ProdutoDao {

    private String TABLE_NAME = "produto";
    private SQLiteDatabase le, escreve;
    private Database db;

    public ProdutoDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(Produto model) {
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("id_estab", model.getIdEstab());
        cv.put("descricao", model.getDescricao());
        cv.put("unidade", model.getUnidade());
        cv.put("preco", model.getPreco());
        cv.put("id_secao", model.getIdSecao());
        cv.put("id_grupo", model.getIdGrupo());
        cv.put("id_departamento", model.getIdDepartamento());
        cv.put("id_marca", model.getIdMarca());
        cv.put("marca", model.getMarca());

        return escreve.insert(TABLE_NAME, null, cv);
    }

    private long atualiza(Produto model) {
        ContentValues cv = new ContentValues();

        cv.put("descricao", model.getDescricao());
        cv.put("unidade", model.getUnidade());
        cv.put("preco", model.getPreco());
        cv.put("id_secao", model.getIdSecao());
        cv.put("id_grupo", model.getIdGrupo());
        cv.put("id_departamento", model.getIdDepartamento());
        cv.put("id_marca", model.getIdMarca());
        cv.put("marca", model.getMarca());

        return escreve.update(TABLE_NAME, cv, "id = ? AND id_estab = ?", new String[]{model.getId(), model.getIdEstab()});
    }

    public long insertOuUpdate(Produto model) {
        //Verifica se o cliente já existe
        Cursor c = le.rawQuery("SELECT id FROM produto WHERE id = ? AND id_estab = ?", new String[]{model.getId(), model.getIdEstab()});
        c.moveToFirst();

        long id = 0;

        if (c.getCount() > 0) {
            //Já existe
            id = atualiza(model);
        } else {
            //Não existe
            id = insere(model);
        }

        c.close();

        return id;
    }

    //Atualiza o peso da unidade e a porcentagem do imposto
    public long atualizaDadosProduto(ProdutoPesoImposto produto){
        ContentValues cv = new ContentValues();

        cv.put("porcent_imposto", produto.getPorcentagemImposto());
        cv.put("peso_unidade", produto.getPesoUnidade());

        return escreve.update(TABLE_NAME, cv, "id = ?", new String[] { String.valueOf(produto.getId()) });
    }

    //O tipoPercentual recebe D para desconto e A para acréscimo
    public List<Produto> lista(String search,
                               String tipoPercentual,
                               double tabPreco,
                               int tipoDepartamento,
                               int idEstab) {

        List<Produto> retorno = new ArrayList<>();

        String sql = "SELECT " +
                "pr.id, " +
                "pr.id_estab, " +
                "pr.descricao, " +
                "pr.unidade, " +
                "pr.preco, " +
                "es.saldo, " +
                "pr.id_secao, " +
                "pr.id_grupo, " +
                "pr.id_departamento, " +
                "COALESCE(pr.id_marca, 0), " +
                "COALESCE(pr.marca, '') AS marca, " +
                "COALESCE(pr.peso_unidade, 0.00), " +
                "COALESCE(pr.porcent_imposto, 0.00) " +
                "FROM " +
                "produto pr " +
                "INNER JOIN estoque es ON pr.id = es.id_item " +
                "WHERE " +
                "(pr.descricao LIKE ? OR pr.id = ? OR pr.marca LIKE ?) AND pr.id_departamento = ? AND pr.preco > 0.0 AND es.id_estab = ?" +
                "ORDER BY " +
                "pr.descricao";

        Cursor c = le.rawQuery(sql, new String[]{ "%" + search + "%", search, search, String.valueOf(tipoDepartamento), String.valueOf(idEstab)});
        c.moveToFirst();

        while (!c.isAfterLast()) {
            Produto model = new Produto();

            model.setId(c.getString(0));
            model.setIdEstab(c.getString(1));
            model.setDescricao(c.getString(2));
            model.setUnidade(c.getString(3));

            //Verifica o tipo da tabela de preço para calcular a %
            double _auxPreco = c.getDouble(4);
            if (tipoPercentual.equals("D")) {
                _auxPreco = _auxPreco - (_auxPreco * (tabPreco / 100));
            } else {
                _auxPreco = _auxPreco + (_auxPreco * (tabPreco / 100));
            }

            model.setPreco(c.getDouble(4));
            model.setEstoque(c.getDouble(5));
            model.setIdSecao(c.getInt(6));
            model.setIdGrupo(c.getInt(7));
            model.setIdDepartamento(c.getInt(8));
            model.setIdMarca(c.getInt(9));
            model.setMarca(c.getString(10));
            model.setPesoUnidade(c.getDouble(11));
            model.setPorcentagemImposto(c.getDouble(12));

            //Multiplica o peso pelo preço
            if(model.getPesoUnidade() > 0.00){
                _auxPreco = _auxPreco * model.getPesoUnidade();
            }

            //Adiciona o valor do imposto
            _auxPreco = _auxPreco + (_auxPreco * (model.getPorcentagemImposto() / 100));

            model.setPrecoLiquido(_auxPreco);

            retorno.add(model);

            c.moveToNext();
        }
        c.close();

        return retorno;
    }

    public List<Produto> listaRelatorioPrecos(String search, @Nullable TabelaPreco tabPreco) {

        List<Produto> retorno = new ArrayList<>();

        String sql = "SELECT " +
                "id, " +
                "id_estab, " +
                "descricao, " +
                "unidade, " +
                "preco, " +
                "0, " +
                "id_secao, " +
                "id_grupo, " +
                "id_departamento, " +
                "COALESCE(id_marca, 0), " +
                "COALESCE(marca, '') AS marca, " +
                "COALESCE(peso_unidade, 0.00), " +
                "COALESCE(porcent_imposto, 0.00) " +
                "FROM " +
                "produto " +
                "WHERE " +
                "(descricao LIKE ? OR id = ? OR marca LIKE ?) AND preco > 0.0 " +
                "ORDER BY " +
                "descricao";

        Cursor c = le.rawQuery(sql, new String[]{ "%" + search + "%", search, search });
        c.moveToFirst();

        while (!c.isAfterLast()) {
            Produto model = new Produto();

            model.setId(c.getString(0));
            model.setIdEstab(c.getString(1));
            model.setDescricao(c.getString(2));
            model.setUnidade(c.getString(3));

            //Verifica o tipo da tabela de preço para calcular a %
            double _auxPreco = c.getDouble(4);

            if(tabPreco != null) {
                if (tabPreco.getTipo().equals("D")) {
                    _auxPreco = _auxPreco - (_auxPreco * (tabPreco.getPercentualPadrao() / 100));
                } else {
                    _auxPreco = _auxPreco + (_auxPreco * (tabPreco.getPercentualPadrao() / 100));
                }
            }

            model.setPreco(c.getDouble(4));
            model.setEstoque(c.getDouble(5));
            model.setIdSecao(c.getInt(6));
            model.setIdGrupo(c.getInt(7));
            model.setIdDepartamento(c.getInt(8));
            model.setIdMarca(c.getInt(9));
            model.setMarca(c.getString(10));

            model.setPesoUnidade(c.getDouble(11));
            model.setPorcentagemImposto(c.getDouble(12));

            //Multiplica o peso pelo preço
            if(model.getPesoUnidade() > 0.00){
                _auxPreco = _auxPreco * model.getPesoUnidade();
            }

            //Adiciona o valor do imposto
            _auxPreco = _auxPreco + (_auxPreco * (model.getPorcentagemImposto() / 100));

            model.setPrecoLiquido(_auxPreco);

            retorno.add(model);

            c.moveToNext();
        }
        c.close();

        return retorno;
    }

    public Produto retorna(int idProduto, int idEstab) {
        Produto model = null;

        String sql = "SELECT " +
                "pr.id, " +
                "pr.id_estab, " +
                "pr.descricao, " +
                "pr.unidade, " +
                "pr.preco, " +
                "es.saldo, " +
                "pr.id_secao, " +
                "pr.id_grupo, " +
                "pr.id_departamento, " +
                "COALESCE(pr.id_marca, 0), " +
                "COALESCE(pr.marca, '') AS marca, " +
                "COALESCE(pr.peso_unidade, 0.00), " +
                "COALESCE(pr.porcent_imposto, 0.00) " +
                "FROM " +
                "produto pr " +
                "INNER JOIN estoque es ON pr.id = es.id_item " +
                "WHERE " +
                "pr.id = ? AND es.id_estab = ? " +
                "ORDER BY " +
                "pr.descricao";

        Cursor c = le.rawQuery(sql, new String[]{ String.valueOf(idProduto), String.valueOf(idEstab)});
        c.moveToFirst();

        if(c.getCount() > 0) {
            model = new Produto();

            model.setId(c.getString(0));
            model.setIdEstab(c.getString(1));
            model.setDescricao(c.getString(2));
            model.setUnidade(c.getString(3));

            model.setPreco(c.getDouble(4));
            model.setEstoque(c.getDouble(5));
            model.setIdSecao(c.getInt(6));
            model.setIdGrupo(c.getInt(7));
            model.setIdDepartamento(c.getInt(8));
            model.setIdMarca(c.getInt(9));
            model.setMarca(c.getString(10));
            model.setPesoUnidade(c.getDouble(11));
            model.setPorcentagemImposto(c.getDouble(12));
            model.setPrecoLiquido(c.getDouble(4));

            c.moveToNext();
        }
        c.close();

        return model;
    }

    public int qtdProdutos(){
        int retorno = 0;

        String sql = "SELECT COUNT(id) FROM produto WHERE preco > 0.0";
        Cursor c = le.rawQuery(sql, null);
        c.moveToFirst();

        if(c.getCount() > 0){
            retorno = c.getInt(0);
        }
        c.close();

        return retorno;
    }
}
