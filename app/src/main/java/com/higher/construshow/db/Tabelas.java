package com.higher.construshow.db;

public class Tabelas {

    public static String TABLE_CIDADE =
            "CREATE TABLE cidade" +
                    "(" +
                    "id         VARCHAR NOT NULL," +
                    "nome       VARCHAR NOT NULL, " +
                    "uf         VARCHAR(2), " +
                    "ibge       VARCHAR)";

    public static String TABLE_CLIENTE =
            "CREATE TABLE cliente" +
                    "(" +
                    "id                     INTEGER NOT NULL," +
                    "nome                   VARCHAR NOT NULL," +
                    "fantasia               VARCHAR," +
                    "id_estab               INTEGER NOT NULL, " +
                    "cnpjf                  VARCHAR NOT NULL," +
                    "email                  VARCHAR," +
                    "id_tipo                VARCHAR(1) NOT NULL, " +
                    "uf                     VARCHAR(2)," +
                    "ativo                  VARCHAR(1)," +
                    "id_conceito            INTEGER, " +
                    "descricao_conceito     VARCHAR, " +
                    "id_regiao              INTEGER, " +
                    "CONSTRAINT pk_cliente PRIMARY KEY (id, id_estab)" +
                    ")";

    public static String TABLE_CONCEITO_CLIENTE =
            "CREATE TABLE conceito_cliente(" +
                    "id                 INTEGER NOT NULL, " +
                    "id_estab           INTEGER NOT NULL, " +
                    "descricao          TEXT NOT NULL, " +
                    "forma_cart         TEXT, " +
                    "forma_dup          TEXT, " +
                    "forma_cheque       TEXT, " +
                    "permite_promocao   VARCHAR(1), " +
                    "CONSTRAINT pk_conceito PRIMARY KEY (id, id_estab)" +
                    ")";

    public static String TABLE_DEPARTAMENTO =
            "CREATE TABLE departamento(" +
                    "id                 INTEGER NOT NULL," +
                    "nome               VARCHAR NOT NULL, " +
                    "desconto_maximo    NUMERIC DEFAULT 0.00, " +
                    "acrescimo_maximo   NUMERIC DEFAULT 0.00" +
                    ")";

    public static String TABLE_ENDERECO =
            "CREATE TABLE endereco" +
                    "(" +
                    "id_cliente     INTEGER NOT NULL, " +
                    "id_endereco    INTEGER NOT NULL, " +
                    "id_estab       INTEGER NOT NULL, " +
                    "tipo_end       VARCHAR(1) NOT NULL," +
                    "endereco       VARCHAR NOT NULL, " +
                    "complemento    VARCHAR," +
                    "numero         VARCHAR, " +
                    "bairro         VARCHAR, " +
                    "id_cidade      VARCHAR, " +
                    "cidade         VARCHAR, " +
                    "cep            VARCHAR, " +
                    "telefone       VARCHAR, " +
                    "uf             VARCHAR(2), " +
                    "CONSTRAINT pk_endereco PRIMARY KEY (id_endereco, id_estab)" +
                    ")";

    public static String TABLE_ESTOQUE =
            "CREATE TABLE estoque(" +
                    "id_estab       INTEGER NOT NULL, " +
                    "id_item        INTEGER NOT NULL, " +
                    "saldo          NUMERIC(18,2) NOT NULL" +
                    ")";

    public static String TABLE_PARCELAS =
            "CREATE TABLE parcelas(" +
                    "id                 INTEGER NOT NULL," +
                    "descricao          TEXT NOT NULL," +
                    "numero_parcelas    INTEGER, " +
                    "carencia           INTEGER)";

    public static String TABLE_PEDIDO =
            "CREATE TABLE pedido" +
                    "(" +
                    "id                 INTEGER NOT NULL, " +
                    "id_estab           INTEGER NOT NULL, " +
                    "id_cliente         INTEGER NOT NULL, " +
                    "id_vendedor        INTEGER NOT NULL, " +
                    "id_endereco        INTEGER NOT NULL, " +
                    "data               VARCHAR NOT NULL," +
                    "qtd_produtos       INTEGER DEFAULT 0, " +
                    "valor_total        NUMERIC DEFAULT 0, " +
                    "valor_liquido      NUMERIC DEFAULT 0, " +
                    "qtd_volume         NUMERIC NOT NULL DEFAULT 0, " +
                    "observacao         VARCHAR," +
                    "id_carrinho        INTEGER, " +
                    "enviado            INTEGER NOT NULL DEFAULT 0, " +
                    "forma_pagamento    TEXT, " +
                    "id_tab_preco       INTEGER NOT NULL, " +
                    "id_parcelamento    INTEGER NOT NULL, " +
                    "valor_flex         NUMERIC NOT NULL DEFAULT 0.00, " +
                    "id_departamento    INTEGER NOT NULL, " +
                    "CONSTRAINT pk_pedido PRIMARY KEY(id, id_estab)" +
                    ")";

    public static String TABLE_PEDIDO_LANCTOS =
            "CREATE TABLE pedido_lancto(" +
                    "id_estab               INTEGER NOT NULL, " +
                    "id_pedido              INTEGER NOT NULL, " +
                    "sequencia              INTEGER NOT NULL, " +
                    "id_produto             INTEGER NOT NULL, " +
                    "id_embalagem           INTEGER NOT NULL, " +
                    "quantidade             NUMERIC NOT NULL, " +
                    "preco                  NUMERIC NOT NULL, " +
                    "desc_item              NUMERIC DEFAULT 0.00, " +
                    "valor_desconto         NUMERIC DEFAULT 0.00, " +
                    "valor_desconto_vis     NUMERIC DEFAULT 0.00, " +
                    "valor_flex_lancto      NUMERIC NOT NULL DEFAULT 0.00, " +
                    "porc_imposto           NUMERIC NOT NULL DEFAULT 0.00, " +
                    "peso_unidade           NUMERIC NOT NULL DEFAULT 0.00, " +
                    "CONSTRAINT pk_pedido_lanctos PRIMARY KEY(id_estab, id_pedido, sequencia)" +
                    ")";

    public static String TABLE_PRODUTO =
            "CREATE TABLE produto" +
                    "(" +
                    "id                 INTEGER NOT NULL," +
                    "id_estab           INTEGER NOT NULL," +
                    "descricao          VARCHAR NOT NULL," +
                    "unidade            VARCHAR NOT NULL," +
                    "preco              VARCHAR NOT NULL," +
                    "id_secao           INTEGER, " +
                    "id_grupo           INTEGER, " +
                    "id_departamento    INTEGER NOT NULL, " +
                    "id_marca           INTEGER, " +
                    "marca              VARCHAR, " +
                    "porcent_imposto    NUMERIC(18,2) DEFAULT 0.00, " +
                    "peso_unidade       NUMERIC(18,2) DEFAULT 0.00, " +
                    "CONSTRAINT pk_produto PRIMARY KEY (id, id_estab)" +
                    ")";

    public static String TABLE_TABELA_PRECO =
            "CREATE TABLE tabela_preco (" +
                    "id                 INTEGER NOT NULL, " +
                    "id_estab           INTEGER NOT NULL, " +
                    "descricao          TEXT NOT NULL, " +
                    "tipo               VARCHAR(5) NOT NULL, " +
                    "perc_padrao        NUMERIC NOT NULL, " +
                    "permite_desconto   VARCHAR(1) NOT NULL, " +
                    "prioriza_promocao  VARCHAR(1) NOT NULL, " +
                    "aplica_em          VARCHAR(1)" +
                    ")";

    public static String TABLE_TIPO_PESSOA =
            "CREATE TABLE tipo_pessoa" +
                    "(" +
                    "id     INTEGER PRIMARY KEY," +
                    "descricao  VARCHAR NOT NULL" +
                    ")";

    public static String TABLE_VENDEDOR =
            "CREATE TABLE vendedor" +
                    "(" +
                    "id         INTEGER PRIMARY KEY," +
                    "id_pessoa  INTEGER, " +
                    "saldo_flex NUMERIC(14,2) NOT NULL DEFAULT 0.00, " +
                    "nome       VARCHAR NOT NULL, " +
                    "email      VARCHAR NOT NULL, " +
                    "user_id    VARCHAR" +
                    ")";

    public static String TABLE_EMPRESA =
            "CREATE TABLE empresa" +
                    "(" +
                    "id                 INTEGER, " +
                    "nome               VARCHAR," +
                    "ultima_sinc        VARCHAR(10) DEFAULT '2010-01-01'," +
                    "ultima_sinc_img    VARCHAR(10) DEFAULT '2010-01-01', " +
                    "ultima_sinc_2      VARCHAR(10) DEFAULT '2010-01-01'" +
                    ")";
}
