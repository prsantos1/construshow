package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.ConceitoCliente;

import java.util.List;

public class ConceitoClienteDao {

    private String TABLE_NAME = "conceito_cliente";
    private SQLiteDatabase le, escreve;
    private Database db;

    public ConceitoClienteDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(ConceitoCliente model) {
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("id_estab", model.getIdEstab());
        cv.put("descricao", model.getDescricao());
        cv.put("forma_cart", model.getFormaCart());
        cv.put("forma_dup", model.getFormaDup());
        cv.put("forma_cheque", model.getFormaCheque());
        cv.put("permite_promocao", model.getPermitePromocao());

        long id = escreve.insert(TABLE_NAME, null, cv);

        return id;
    }

    public long atualiza(ConceitoCliente model) {
        ContentValues cv = new ContentValues();

        cv.put("descricao", model.getDescricao());
        cv.put("forma_cart", model.getFormaCart());
        cv.put("forma_dup", model.getFormaDup());
        cv.put("forma_cheque", model.getFormaCheque());
        cv.put("permite_promocao", model.getPermitePromocao());

        long id = escreve.update(TABLE_NAME, cv, "id = ? AND id_estab = ?", new String[]{String.valueOf(model.getId()), String.valueOf(model.getIdEstab())});

        return id;
    }

    public long insertOuUpdate(ConceitoCliente model) {

        Cursor c = le.rawQuery("SELECT id FROM conceito_cliente WHERE id = ? AND id_estab = ?", new String[]{String.valueOf(model.getId()), String.valueOf(model.getIdEstab())});
        c.moveToFirst();

        long id = 0;

        if (c != null) {
            if (c.getCount() > 0) {
                //Já existe
                id = atualiza(model);
            } else {
                //Não existe
                id = insere(model);
            }
        }
        c.close();

        return id;
    }

    public ConceitoCliente retorna(String id, String idEstab) {
        ConceitoCliente retorno = new ConceitoCliente();

        String sql = "SELECT " +
                        "id, " +
                        "id_estab, " +
                        "descricao, " +
                        "forma_cart," +
                        "forma_dup, " +
                        "forma_cheque, " +
                        "permite_promocao " +
                    "FROM " +
                        "conceito_cliente " +
                    "WHERE " +
                        "id = ? AND id_estab = ?";

        Cursor c = le.rawQuery(sql, new String[] {id, idEstab });
        c.moveToFirst();

        if(c.getCount() > 0){
            retorno.setId(c.getInt(0));
            retorno.setIdEstab(c.getInt(1));
            retorno.setDescricao(c.getString(2));
            retorno.setFormaCart(c.getString(3));
            retorno.setFormaDup(c.getString(4));
            retorno.setFormaCheque(c.getString(5));
            retorno.setPermitePromocao(c.getString(6));
        }else{
            retorno = null;
        }
        c.close();

        return retorno;
    }
}
