package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.higher.construshow.model.PedidoLancto;

import java.util.ArrayList;
import java.util.List;

public class PedidoLanctoDao {

    private String TABLE_NAME = "pedido_lancto";
    private SQLiteDatabase le, escreve;
    private Database db;

    public PedidoLanctoDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(PedidoLancto model) {
        ContentValues cv = new ContentValues();

        cv.put("id_estab", model.getIdEstab());
        cv.put("id_pedido", model.getIdPedido());
        cv.put("sequencia", model.getSequencia());
        cv.put("id_produto", model.getIdProduto());
        cv.put("id_embalagem", model.getIdEmbalagem());
        cv.put("quantidade", model.getQuantidade());
        cv.put("preco", model.getPreco());
        cv.put("desc_item", model.getDescItem());
        cv.put("valor_desconto", model.getValorDesconto());
        cv.put("valor_desconto_vis", model.getValorDescontoVisualizacao());
        cv.put("valor_flex_lancto", model.getValorFlexProduto());
        cv.put("porc_imposto", model.getPorcentagemImposto());
        cv.put("peso_unidade", model.getPesoUnidade());

        long id = escreve.insert(TABLE_NAME, null, cv);

        //Depois de inserir ele atualiza o pedido com o valor líquido
        double _totalLiquido = valorTotalLiquido(String.valueOf(model.getIdPedido()));

        //Depois de inserir ele atualiza o pedido com o valor total
        double _totalPedido = valorTotalPedido(String.valueOf(model.getIdPedido()));
        escreve.execSQL("UPDATE pedido SET valor_total = " + _totalPedido + ", valor_liquido = " + _totalLiquido + ", valor_flex = valor_flex + " + model.getValorFlexProduto() + " WHERE id = " + model.getIdPedido() + " AND id_estab = " + model.getIdEstab());

        //Atualiza com a quantidade de produtos atual do pedido
        int _qtdTotal = qtdTotalProdutos(String.valueOf(model.getIdPedido()));
        escreve.execSQL("UPDATE pedido SET qtd_produtos = " + _qtdTotal + " WHERE id = " + model.getIdPedido() + " AND id_estab = " + model.getIdEstab());

        return id;
    }

    public int atualiza(PedidoLancto model) {
        ContentValues cv = new ContentValues();

        cv.put("quantidade", model.getQuantidade());
        cv.put("desc_item", model.getDescItem());
        cv.put("valor_desconto", model.getValorDesconto());
        cv.put("valor_desconto_vis", model.getValorDescontoVisualizacao());
        cv.put("porc_imposto", model.getPorcentagemImposto());
        cv.put("peso_unidade", model.getPesoUnidade());

        int id = escreve.update(
                TABLE_NAME,
                cv,
                "id_pedido = ? AND id_produto = ?",
                new String[]{
                        String.valueOf(model.getIdPedido()),
                        String.valueOf(model.getIdProduto())
                });

        if (id > 0) {

            //Depois de atualizar ele atualiza o pedido com o valor líquido
            double _totalLiquido = valorTotalLiquido(String.valueOf(model.getIdPedido()));

            //Depois de atualizar ele atualiza o pedido com o valor total
            double _totalPedido = valorTotalPedido(String.valueOf(model.getIdPedido()));
            escreve.execSQL("UPDATE pedido SET valor_total = " + _totalPedido + ", valor_liquido = " + _totalLiquido + ", valor_flex = valor_flex + " + model.getValorFlexProduto() + " WHERE id = " + model.getIdPedido() + " AND id_estab = " + model.getIdEstab());
        }

        return id;
    }

    public long save(PedidoLancto model) {
        long id = 0;

        String sql = "SELECT id_pedido FROM pedido_lancto WHERE id_pedido = ? AND id_produto = ?";
        Cursor c = le.rawQuery(sql, new String[]{String.valueOf(model.getIdPedido()), String.valueOf(model.getIdProduto())});
        c.moveToFirst();

        if (c.getCount() > 0) {
            Log.d("save", "update");
            id = atualiza(model);
        } else {
            Log.d("save", "insert");
            id = insere(model);
        }
        c.close();

        return id;
    }

    public List<PedidoLancto> lista(String idPedido) {
        List<PedidoLancto> _lanctos = new ArrayList<>();

        String sql =
                "SELECT " +
                        "pl.id_estab, " +
                        "pl.id_pedido, " +
                        "pl.sequencia, " +
                        "pl.id_produto, " +
                        "pl.id_embalagem, " +
                        "pl.quantidade, " +
                        "ROUND(pl.preco, 4), " +
                        "ROUND(pl.desc_item, 4), " +
                        "ROUND(pl.valor_desconto, 4), " +
                        "pr.descricao, " +
                        "tb.perc_padrao, " +
                        "tb.tipo, " +
                        "ROUND(pl.valor_flex_lancto, 4), " +
                        "ROUND(pl.porc_imposto, 4), " +
                        "COALESCE(pl.peso_unidade, 0.00), " +
                        "ROUND(pl.valor_desconto_vis) " +
                        "FROM " +
                        "pedido_lancto pl " +
                        "INNER JOIN produto pr ON pl.id_produto = pr.id " +
                        "INNER JOIN pedido pe ON pl.id_pedido = pe.id " +
                        "INNER JOIN tabela_preco tb ON pe.id_tab_preco = tb.id " +
                        "WHERE " +
                        "pl.id_pedido = ?";

        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        if (c.getCount() > 0) {
            while (!c.isAfterLast()) {
                PedidoLancto model = new PedidoLancto();

                model.setIdEstab(c.getInt(0));
                model.setIdPedido(c.getInt(1));
                model.setSequencia(c.getInt(2));
                model.setIdProduto(c.getInt(3));
                model.setIdEmbalagem(c.getInt(4));
                model.setQuantidade(c.getDouble(5));
                model.setPreco(c.getDouble(6));
                model.setDescItem(c.getDouble(7));
                model.setNomeProduto(c.getString(9));
                model.setValorFlexProduto(c.getDouble(12));
                model.setPorcentagemImposto(c.getDouble(13));
                model.setPesoUnidade(c.getDouble(14));
                model.setValorDescontoVisualizacao(c.getDouble(15));

                //Calcula o valor do desconto total
                double _auxDescTotal = 0.00;
                double _auxPrecoLiquido = 0.00;

                _auxDescTotal = (c.getDouble(8) * model.getQuantidade());

                model.setValorDesconto(_auxDescTotal);
                model.setValorDescontoUnitario(c.getDouble(8));

                //Calcula o preço líquido (preco - desconto %)
                _auxPrecoLiquido = model.getPreco() - (model.getPreco() * (model.getDescItem() / 100));

                //Calcula valor total do lancto (preço liquido x quantidade)
                double _auxPreco = _auxPrecoLiquido * model.getQuantidade();

                //Calcula a tabela de preço
                if (c.getString(11).equals("A")) {
                    _auxPreco += _auxPreco * (c.getDouble(10) / 100);
                    _auxPrecoLiquido += _auxPrecoLiquido * (c.getDouble(10) / 100);
                } else {
                    _auxPreco -= _auxPreco * (c.getDouble(10) / 100);
                    _auxPrecoLiquido -= _auxPrecoLiquido * (c.getDouble(10) / 100);
                }

                model.setPrecoLiquido(_auxPrecoLiquido);

                if(model.getPesoUnidade() > 0.00){
                    //calcula a quantidade final (quantidade * peso da unidade)
                    model.setQuantidadeFinal(model.getQuantidade() * model.getPesoUnidade());
                }else{
                    model.setQuantidadeFinal(model.getQuantidade());
                }

                //calcula o valor do desconto final (valor do desconto * peso da unidade)
                model.setValorDescontoFinal(model.getValorDesconto() * model.getPesoUnidade());

                /*
                 Os cálculos abaixos serão aplicados apenas para visualização no aparelho
                 Vão para o campo precoVisualizacao
                 */

                //Multiplica pelo peso caso ele seja maior que 0.00
                if (model.getPesoUnidade() > 0.00) {
                    _auxPreco = _auxPreco * model.getPesoUnidade();
                    _auxPrecoLiquido = _auxPrecoLiquido * model.getPesoUnidade();
                }

                //Calcula o valor do imposto do produto (%)
                _auxPreco += _auxPreco * (model.getPorcentagemImposto() / 100);
                _auxPrecoLiquido += _auxPrecoLiquido * (model.getPorcentagemImposto() / 100);

                model.setPrecoVisualizacao(_auxPrecoLiquido);
                model.setValorTotal(_auxPreco);

                /*
                Fim dos calculos para visualização
                 */

                _lanctos.add(model);
                c.moveToNext();
            }
        }
        c.close();

        return _lanctos;
    }

    public int nextSeq(String idPedido) {
        int _retorno = 0;

        String sql = "SELECT MAX(sequencia) FROM pedido_lancto WHERE id_pedido = ?";
        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        if (c.getCount() > 0) {
            _retorno = c.getInt(0);

            _retorno++;
        }
        c.close();

        return _retorno;
    }

    public double valorTotalLiquido(String idPedido) {
        double retorno = 0.00;

        String sql = "SELECT preco, desc_item, quantidade, peso_unidade FROM pedido_lancto WHERE id_pedido = ?";
        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        double _aux = 0.00;

        while (!c.isAfterLast()) {
            _aux = c.getDouble(0);
            //Aplica o desconto dado do item
            _aux = _aux - (_aux * (c.getDouble(1) / 100));
            //multiplica pela quantidade
            _aux = _aux * c.getDouble(2);
            //multiplica pelo peso somente se for maior que zero
            if(c.getDouble(3) > 0.00) _aux = _aux * c.getDouble(3);

            retorno += _aux;

            c.moveToNext();
        }
        c.close();

        return retorno;
    }

    public double valorTotalPedido(String idPedido) {
        double retorno = 0.00;

        //String sql = "SELECT SUM(((preco - valor_desconto) * quantidade)) FROM pedido_lancto WHERE id_pedido = ?";
        String sql = "SELECT preco, desc_item, quantidade, porc_imposto, peso_unidade FROM pedido_lancto WHERE id_pedido = ?";
        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        while (!c.isAfterLast()) {
            double _auxPreco = c.getDouble(0);

            //Abate o desconto
            _auxPreco = _auxPreco - (_auxPreco * (c.getDouble(1) / 100));

            //Multiplica pelo peso
            if (c.getDouble(4) > 0.00) _auxPreco = _auxPreco * c.getDouble(4);

            //Multiplica pela quantidade
            _auxPreco = _auxPreco * c.getDouble(2);

            //Round no preço para 2 casas decimais para ficar igual ao Construshow
            _auxPreco = (double) Math.round(_auxPreco * 100) / 100;

            //Calcula o valor do imposto do produto (%)
            _auxPreco += _auxPreco * (c.getDouble(3) / 100);

            _auxPreco = (double) Math.round(_auxPreco * 100) / 100;

            retorno += _auxPreco;

            c.moveToNext();
        }
        c.close();

        return retorno;
    }

    private int qtdTotalProdutos(String idPedido) {
        int retorno = 0;

        String sql = "SELECT COUNT(sequencia) FROM pedido_lancto WHERE id_pedido = ?";

        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        if (c.getCount() > 0) {
            retorno = c.getInt(0);
        }
        c.close();

        return retorno;
    }

    public void excluiLancto(String idPedido, String sequencia, double valorFlexLancto) {
        escreve.execSQL("DELETE FROM pedido_lancto WHERE id_pedido = " + idPedido + " AND sequencia = " + sequencia);

        //Depois de excluir ele atualiza o valor total do pedido e o valor da verba do pedido
        double _totalPedido = valorTotalPedido(idPedido);
        double _totalLiquido = valorTotalLiquido(idPedido);

        escreve.execSQL(
                "UPDATE pedido SET valor_total = " + _totalPedido + ", valor_liquido = " + _totalLiquido + ", valor_flex = valor_flex - " + valorFlexLancto + " WHERE id = " + idPedido);
    }
}
