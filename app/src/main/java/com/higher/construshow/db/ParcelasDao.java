package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.higher.construshow.model.Parcela;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ParcelasDao {

    private String TABLE_NAME = "parcelas";
    private SQLiteDatabase le, escreve;
    private Database db;

    public ParcelasDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }


    public long insere(Parcela model) {
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("descricao", model.getDescricao());
        cv.put("numero_parcelas", model.getNumeroParcelas());
        cv.put("carencia", model.getCarencia());

        long id = escreve.insert(TABLE_NAME, null, cv);

        return id;
    }

    public long atualiza(Parcela model) {
        ContentValues cv = new ContentValues();

        cv.put("descricao", model.getDescricao());
        cv.put("numero_parcelas", model.getNumeroParcelas());
        cv.put("carencia", model.getCarencia());

        long id = escreve.update(TABLE_NAME, cv, "id = ?", new String[] { String.valueOf(model.getId()) });

        return id;
    }


    public long insertOuUpdate(Parcela model) {

        Cursor c = le.rawQuery("SELECT id FROM parcelas WHERE id = ?", new String[]{String.valueOf(model.getId())});
        c.moveToFirst();

        long id = 0;

        if (c != null) {
            if (c.getCount() > 0) {
                //Já existe
                id = atualiza(model);
            } else {
                //Não existe
                id = insere(model);
            }
        }
        c.close();

        return id;
    }


    public Parcela retorna(int id){
        Parcela parcela = new Parcela();

        String sql = "SELECT " +
                "id, " +
                "descricao, " +
                "numero_parcelas, " +
                "carencia " +
                "FROM " +
                "parcelas " +
                "WHERE " +
                "id = ?";

        Cursor c = le.rawQuery(sql, new String[] { String.valueOf(id) });
        c.moveToFirst();

        if(c.getCount() > 0){
            parcela.setId(c.getInt(0));
            parcela.setDescricao(c.getString(1));
            parcela.setNumeroParcelas(c.getInt(2));
            parcela.setCarencia(c.getInt(3));
        }else{
            parcela = null;
        }
        c.close();

        return parcela;
    }

    public List<Parcela> todos(String idList){
        List<Parcela> retorno = new ArrayList<>();

        String sql = "SELECT " +
                        "id, " +
                        "descricao, " +
                        "numero_parcelas, " +
                        "carencia " +
                    "FROM " +
                        "parcelas " +
                    "WHERE " +
                        "id IN (" + idList + ")";

        Cursor c = le.rawQuery(sql, null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            Parcela parcela = new Parcela();

            parcela.setId(c.getInt(0));
            parcela.setDescricao(c.getString(1));
            parcela.setNumeroParcelas(c.getInt(2));
            parcela.setCarencia(c.getInt(3));

            retorno.add(parcela);

            c.moveToNext();
        }
        c.close();

        return retorno;
    }
}
