package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Cidade;

import java.util.ArrayList;
import java.util.List;

public class CidadeDao {

    private String TABLE_NAME = "cidade";
    private SQLiteDatabase le, escreve;
    private Database db;

    public CidadeDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(Cidade model){
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("nome", model.getNome());
        cv.put("uf", model.getUf());
        cv.put("ibge", model.getIbge());

        return escreve.insert(TABLE_NAME, null, cv);
    }

    public List<Cidade> lista(String pesquisa){
        List<Cidade> retorno = new ArrayList<>();

        String sql = "SELECT id, nome, uf, ibge FROM cidade WHERE nome LIKE ?";
        Cursor c = le.rawQuery(sql, new String[]{pesquisa + "%"});
        c.moveToFirst();

        while(!c.isAfterLast()){
            Cidade model = new Cidade();

            model.setId(c.getString(0));
            model.setNome(c.getString(1));
            model.setUf(c.getString(2));
            model.setIbge(c.getString(3));

            retorno.add(model);
            c.moveToNext();
        }
        c.close();

        return retorno;
    }
}
