package com.higher.construshow.view;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.higher.construshow.R;
import com.higher.construshow.adapter.PedidoLanctoAdapter;
import com.higher.construshow.db.PedidoLanctoDao;
import com.higher.construshow.model.Pedido;
import com.higher.construshow.model.PedidoLancto;
import com.higher.construshow.util.Repositorio;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class PedidoEnviadoView extends AppCompatActivity {

    @BindView(R.id.txvCliente)
    TextView txvCliente;
    @BindView(R.id.txvNumeroControle)
    TextView txvNumeroControle;
    @BindView(R.id.txvDataPedido)
    TextView txvDataPedido;
    @BindView(R.id.txvTotal)
    TextView txvTotal;
    @BindView(R.id.txvObservacao)
    TextView txvObservacao;

    private Pedido pedido = null;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_enviado_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        pedido = (Pedido) getIntent().getSerializableExtra("pedido");

        if(pedido != null){
            txvCliente.setText(pedido.getIdCliente() + " - " + pedido.getNomeCliente());
            txvNumeroControle.setText("Número do carrinho: " + pedido.getIdCarrinho());
            txvDataPedido.setText(Repositorio.dataBancoParaTela(pedido.getData()));
            txvTotal.setText(pedido.getQtdProdutos() + " produtos e R$ " + Repositorio.formataNumero(pedido.getValorTotal()));
            txvObservacao.setText(pedido.getObservacao());

            carregaLista();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pedido_enviado, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }else if(item.getItemId() == R.id.duplicarPedido){
            Repositorio.exibeDialogDuplicar(PedidoEnviadoView.this, String.valueOf(pedido.getId()));
        }
        return super.onOptionsItemSelected(item);
    }

    private void carregaLista(){
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(PedidoEnviadoView.this);
        recyclerView.setLayoutManager(mLayoutManager);

        PedidoLanctoDao pedidoLanctoDao = new PedidoLanctoDao(getApplicationContext());

        List<PedidoLancto> lanctoList = pedidoLanctoDao.lista(String.valueOf(pedido.getId()));

        PedidoLanctoAdapter pedidoLanctoAdapter = new PedidoLanctoAdapter(lanctoList);
        recyclerView.setAdapter(pedidoLanctoAdapter);

    }
}
