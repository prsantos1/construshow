package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;
import com.higher.construshow.R;
import com.higher.construshow.adapter.CidadesAdapter;
import com.higher.construshow.db.CidadeDao;
import com.higher.construshow.model.Cidade;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CidadesView extends AppCompatActivity {

    @BindView(R.id.edtPesquisa)
    TextInputEditText edtPesquisa;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private RecyclerView.LayoutManager mLayoutManager;
    private CidadesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cidades_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(CidadesView.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setVerticalScrollBarEnabled(true);
        recyclerView.setVerticalFadingEdgeEnabled(true);

        carregaLista();

        edtPesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                carregaLista();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void carregaLista(){
        CidadeDao cidadeDao = new CidadeDao(CidadesView.this);
        List<Cidade> lista = cidadeDao.lista(edtPesquisa.getText().toString());
        adapter = new CidadesAdapter(lista);

        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener((adapter1, view, position) -> {
            setResult(RESULT_OK, new Intent().putExtra("cidade", lista.get(position)));
            finish();
        });
    }
}
