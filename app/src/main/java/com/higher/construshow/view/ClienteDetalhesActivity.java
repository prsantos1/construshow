package com.higher.construshow.view;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.higher.construshow.R;
import com.higher.construshow.adapter.EnderecoAdapter;
import com.higher.construshow.db.EnderecoDao;
import com.higher.construshow.model.Cliente;
import com.higher.construshow.model.Endereco;
import com.higher.construshow.util.Repositorio;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClienteDetalhesActivity extends AppCompatActivity {

    private Cliente cliente;

    @BindView(R.id.txvRazaoSocial)
    TextView txvNome;
    @BindView(R.id.txvNomeFantasia)
    TextView txvFantasia;
    @BindView(R.id.txvCnpjf)
    TextView txvCnpjf;
    @BindView(R.id.txvDadosRazao)
    TextView txvDadosRazao;
    @BindView(R.id.txvDadosFantasia)
    TextView txvDadosFantasia;
    @BindView(R.id.txvDadosCnpjf)
    TextView txvDadosCnpjf;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txvEmpresaOuNome)
    TextView txvEmpresaOuNome;
    @BindView(R.id.txvCnpjOuCpf)
    TextView txvCnpjOuCpf;

    private RecyclerView.LayoutManager mLayoutManager;
    private List<Endereco> listaEnderecos;
    private EnderecoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_detalhes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        cliente = (Cliente) getIntent().getSerializableExtra("cliente");

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ClienteDetalhesActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setVerticalScrollBarEnabled(true);
        recyclerView.setVerticalFadingEdgeEnabled(true);

        if(cliente != null){

            //verifica se é cnpj ou cpf
            if(cliente.getCnpjf().length() < 14){
                txvCnpjOuCpf.setText("CPF");
                txvEmpresaOuNome.setText("Nome");
            }else{
                txvCnpjOuCpf.setText("CNPJ");
                txvEmpresaOuNome.setText("Empresa");
            }

            txvNome.setText(cliente.getNome());
            txvFantasia.setText(cliente.getFantasia());
            txvCnpjf.setText( Repositorio.formataCnpjCpf(cliente.getCnpjf()) );

            txvDadosRazao.setText(cliente.getNome());
            txvDadosFantasia.setText(cliente.getFantasia());
            txvDadosCnpjf.setText( Repositorio.formataCnpjCpf(cliente.getCnpjf()) );

            carregaListaEnderecos();
        }else{
            Toast.makeText(ClienteDetalhesActivity.this, "Houve um erro ao selecionar o cliente, tente novamente", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void carregaListaEnderecos(){
        EnderecoDao enderecoDao = new EnderecoDao(getApplicationContext());
        listaEnderecos = enderecoDao.lista(cliente.getId());

        adapter = new EnderecoAdapter(listaEnderecos);
        adapter.openLoadAnimation();
        recyclerView.setAdapter(adapter);
    }
}
