package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.higher.construshow.R;
import com.higher.construshow.adapter.ParcelamentosAdapter;
import com.higher.construshow.db.ConceitoClienteDao;
import com.higher.construshow.db.ParcelasDao;
import com.higher.construshow.model.Cliente;
import com.higher.construshow.model.ConceitoCliente;
import com.higher.construshow.model.Parcela;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelecionaParcelamentoView extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.edtPesquisa)
    TextInputEditText edtPesquisa;

    List<Parcela> parcelas = new ArrayList<>();

    ParcelamentosAdapter adapter;

    private RecyclerView.LayoutManager mLayoutManager;

    private Cliente cliente = null;
    private ConceitoCliente conceitoCliente = null;

    private String formaPagamento = "";

    List<String> parcelasId = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleciona_parcelamento_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        cliente = (Cliente) getIntent().getSerializableExtra("cliente");
        formaPagamento = getIntent().getStringExtra("forma_pagamento");

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(SelecionaParcelamentoView.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setVerticalScrollBarEnabled(true);
        recyclerView.setVerticalFadingEdgeEnabled(true);

        getConceito();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void carregaLista(String idList) {
        ParcelasDao parcelasDao = new ParcelasDao(getApplicationContext());

        parcelas = parcelasDao.todos(idList);

        adapter = new ParcelamentosAdapter(parcelas);
        adapter.openLoadAnimation();

        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener((adapter1, view, position) -> {
            Parcela _parcela = parcelas.get(position);

            setResult(RESULT_OK, new Intent().putExtra("parcelamento", _parcela));
            finish();
        });
    }

    private void getConceito() {
        if (cliente != null) {
            ConceitoClienteDao conceitoClienteDao = new ConceitoClienteDao(getApplicationContext());
            conceitoCliente = conceitoClienteDao.retorna(cliente.getIdConceito(), cliente.getIdEstab());

            if(conceitoCliente != null) {
                //Verifica se a forma de pagamento foi cartão / boleto / cheque
                switch (formaPagamento) {
                    case "Cartão de crédito":
                        carregaLista(conceitoCliente.getFormaCart());
                        break;
                    case "Boleto/Duplicata":
                        carregaLista(conceitoCliente.getFormaDup());
                        break;
                    case "Cheque":
                        carregaLista(conceitoCliente.getFormaCheque());
                        break;
                }

            }else{
                Log.d("nit", "nao tem conceito" + cliente.getIdEstab());
            }

        } else {
            Toast.makeText(getApplicationContext(), "Houve um erro ao obter os dados, tente novamente", Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
