package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.higher.construshow.R;
import com.higher.construshow.db.DepartamentoDao;
import com.higher.construshow.db.PedidoLanctoDao;
import com.higher.construshow.db.ProdutoDao;
import com.higher.construshow.db.TabelaPrecoDao;
import com.higher.construshow.model.Departamento;
import com.higher.construshow.model.Pedido;
import com.higher.construshow.model.PedidoLancto;
import com.higher.construshow.model.Produto;
import com.higher.construshow.model.TabelaPreco;
import com.higher.construshow.util.Repositorio;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PedidoLanctoActivity extends AppCompatActivity {

    @BindView(R.id.linearProduto)
    LinearLayout linearProduto;
    @BindView(R.id.txvProduto)
    TextView txvProduto;

    @BindView(R.id.txvCodProduto)
    TextView txvCodProduto;
    @BindView(R.id.txvUnidade)
    TextView txvUnidade;
    @BindView(R.id.txvPromocao)
    TextView txvPromocao;
    @BindView(R.id.txvPesoUnidade)
    TextView txvPesoUnidade;
    @BindView(R.id.txvEstoque)
    TextView txvEstoque;
    @BindView(R.id.txvTabPreco)
    TextView txvTabPreco;
    @BindView(R.id.txvPrecoLiquido)
    TextView txvPrecoLiquido;

    @BindView(R.id.txvDescontoMaximo)
    TextView txvDescontoMaximo;

    @BindView(R.id.txvQuantidade)
    TextView txvQuantidade;
    @BindView(R.id.txvDesconto)
    TextView txvDesconto;
    @BindView(R.id.txvValorDesconto)
    TextView txvValorDesconto;

    @BindView(R.id.btnAltQtd)
    Button btnAltQtd;
    @BindView(R.id.btnAltDesconto)
    Button btnAltDesconto;
    @BindView(R.id.btnAltValorDesc)
    Button btnAltValorDesc;

    @BindView(R.id.txvValorTotal)
    TextView txvValorTotal;

    private Produto produto;
    private Pedido pedido;
    private PedidoLancto pedidoLancto;

    private TabelaPreco tabelaPreco = null;
    private int idDepartamento;
    private Departamento departamento = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_lancto);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        produto = null;
        pedido = null;
        pedidoLancto = null;

        pedidoLancto = (PedidoLancto) getIntent().getSerializableExtra("lancto");
        pedido = (Pedido) getIntent().getSerializableExtra("pedido");
        idDepartamento = getIntent().getIntExtra("departamento", 0);

        if (pedido == null) {
            Toast.makeText(getApplicationContext(), "Houve um erro ao selecionar o produto, tente novamente", Toast.LENGTH_LONG).show();
            finish();
        }

        departamento = carregaDepartamento();
        if (departamento != null)
            txvDescontoMaximo.setText("Desconto máximo permitido: " + Repositorio.formataNumero(departamento.getDescontoMaximo()) + "%");

        if (pedidoLancto == null) {
            pedidoLancto = new PedidoLancto();

            produto = (Produto) getIntent().getSerializableExtra("produto");
            tabelaPreco = (TabelaPreco) getIntent().getSerializableExtra("tabela_preco");

            if (produto != null && tabelaPreco != null) {
                carregaComponentes();

            } else {
                Toast.makeText(getApplicationContext(), "Houve um erro ao selecionar o produto, tente novamente", Toast.LENGTH_LONG).show();
                finish();
            }
        } else {
            produto = carregaProduto();
            tabelaPreco = carregaTabPreco();
            carregaComponentes();

            txvDesconto.setText(Repositorio.formataNumero(pedidoLancto.getDescItem()));
            txvQuantidade.setText(Repositorio.formataNumero(pedidoLancto.getQuantidade()));
            txvValorDesconto.setText(Repositorio.formataNumero(pedidoLancto.getValorDesconto()));

            calculaValorDesconto();
            calculaPrecos();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> gravar());

        btnAltDesconto.setOnClickListener(view -> {
            Intent i = new Intent(getApplicationContext(), SelecionaValorActivity.class);
            i.putExtra("valor_tela", txvDesconto.getText().toString());
            i.putExtra("permite_virgula", 1);
            startActivityForResult(i, 1);
        });

        btnAltQtd.setOnClickListener(view -> {
            Intent i = new Intent(getApplicationContext(), SelecionaValorActivity.class);
            i.putExtra("valor_tela", txvQuantidade.getText().toString());
            i.putExtra("permite_virgula", 0);
            startActivityForResult(i, 2);
        });

        btnAltValorDesc.setOnClickListener(view -> {
            Intent i = new Intent(getApplicationContext(), SelecionaValorActivity.class);
            i.putExtra("valor_tela", txvValorDesconto.getText().toString());
            i.putExtra("permite_virgula", 1);
            startActivityForResult(i, 3);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                txvDesconto.setText("0,00");
            } else {
                txvDesconto.setText(Repositorio.formataNumero(Double.valueOf(data.getAction().replaceAll(",", "."))));
            }

            calculaValorDesconto();
            calculaPrecos();
        } else if (requestCode == 2) {
            if (resultCode == RESULT_CANCELED) {
                txvQuantidade.setText("1");
            } else {
                txvQuantidade.setText(data.getAction());
            }

            calculaPrecos();

        } else if (requestCode == 3) {
            if (resultCode == RESULT_CANCELED) {
                txvValorDesconto.setText("0,00");
            } else {
                txvValorDesconto.setText(Repositorio.formataNumero(Double.valueOf(data.getAction().replaceAll(",", "."))));
            }

            calculaDescontoPorcentagem();
            calculaPrecos();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void carregaComponentes() {
        txvProduto.setText(produto.getDescricao());
        txvCodProduto.setText(produto.getId());
        txvUnidade.setText(produto.getUnidade());
        txvPesoUnidade.setText(Repositorio.formataNumero(produto.getPesoUnidade()));
        txvEstoque.setText(Repositorio.formataNumero(produto.getEstoque()));

        txvTabPreco.setText(
                tabelaPreco.getTipo().equals("A") ?
                        "+ " + Repositorio.formataNumero(tabelaPreco.getPercentualPadrao()) + "%" :
                        "- " + Repositorio.formataNumero(tabelaPreco.getPercentualPadrao()) + "%"
        );

        //calcula o preço líquido inicial de acordo com a tabela de preço
        double _auxPreco = produto.getPreco();
        if (tabelaPreco.getTipo().equals("A")) {
            _auxPreco += _auxPreco * (tabelaPreco.getPercentualPadrao() / 100);
        } else {
            _auxPreco -= _auxPreco * (tabelaPreco.getPercentualPadrao() / 100);
        }

        //Multiplica pelo peso da unidade somente se o peso for maior que 0.00
        if (produto.getPesoUnidade() > 0.00) _auxPreco = _auxPreco * produto.getPesoUnidade();

        //Adiciona o valor do imposto %
        _auxPreco += _auxPreco * (produto.getPorcentagemImposto() / 100);

        txvPrecoLiquido.setText(Repositorio.formataNumero(_auxPreco));
        txvValorTotal.setText("R$ " + Repositorio.formataNumero(_auxPreco));
    }

    private void calculaPrecos() {
        double _desconto = 0.00;
        double _preco = 0.00;
        double _valorTotal = 0.00;
        double _auxQtd = 0.00;

        _auxQtd = Double.valueOf(txvQuantidade.getText().toString().replace(".", "").replace(",", "."));
        _desconto = Double.valueOf(txvDesconto.getText().toString().replace(".", "").replace(",", "."));

        //Calcula o preço líquido
        _preco = produto.getPreco() - (produto.getPreco() * (_desconto / 100));

        //Calcula o preço de acordo com a tabela de preço
        if (tabelaPreco.getTipo().equals("A")) {
            _preco += _preco * (tabelaPreco.getPercentualPadrao() / 100);
        } else {
            _preco -= _preco * (tabelaPreco.getPercentualPadrao() / 100);
        }

        //Multiplica o preço bruto pelo peso caso o peso seja maior que 0.00
        if (produto.getPesoUnidade() > 0.00) _preco = _preco * produto.getPesoUnidade();

        //Calcula o valor do imposto do produto (%)
        _preco += _preco * (produto.getPorcentagemImposto() / 100);
        _valorTotal = _preco * _auxQtd;

        txvPrecoLiquido.setText(Repositorio.formataNumero(_preco));

        txvValorTotal.setText("R$ " + Repositorio.formataNumero(_valorTotal));
    }

    private void calculaValorDesconto() {
        double _auxDesconto = 0.00;
        double _precoBruto = 0.00;
        double _preco = 0.00;
        double _valorDesconto = 0.00;

        _auxDesconto = Double.valueOf(txvDesconto.getText().toString().replace(".", "").replace(",", "."));

        //O preço bruto é antes de aplicar a quantidade e o desconto
        _precoBruto = produto.getPreco();

        //Calcula a tabela de preço em cima do preço bruto
        //Checa se é acréscimo ou desconto
        if (tabelaPreco.getTipo().equals("A")) {
            _precoBruto += _precoBruto * (tabelaPreco.getPercentualPadrao() / 100);
        } else {
            _precoBruto -= _precoBruto * (tabelaPreco.getPercentualPadrao() / 100);
        }

        //Abate o desconto
        _preco = _precoBruto - ((_precoBruto * _auxDesconto) / 100);
        //Obtem o valor do desconto que vai para o pedido
        _valorDesconto = _precoBruto - _preco;
        pedidoLancto.setValorDesconto(_valorDesconto);

        /*
        A partir daqui ele calcula o valor do desconto apenas para visualização
         */

        //Multiplica o preço bruto pelo peso caso o peso seja maior que 0.00
        if (produto.getPesoUnidade() > 0.00) _precoBruto = _precoBruto * produto.getPesoUnidade();

        //Calcula o valor do imposto do produto (%)
        _precoBruto += _precoBruto * (produto.getPorcentagemImposto() / 100);

        //Abate o desconto
        _preco = _precoBruto - ((_precoBruto * _auxDesconto) / 100);

        _valorDesconto = _precoBruto - _preco;

        txvValorDesconto.setText(Repositorio.formataNumero(_valorDesconto));
    }

    private void calculaDescontoPorcentagem() {
        double _desconto = 0.00;
        double _precoBruto = 0.00;
        double _valorDesconto = 0.00;

        _valorDesconto = Double.valueOf(txvValorDesconto.getText().toString().replace(".", "").replace(",", "."));

        _precoBruto = produto.getPreco();

        //Calcula a tabela de preço em cima do preço bruto
        //Checa se é acréscimo ou desconto
        if (tabelaPreco.getTipo().equals("A")) {
            _precoBruto += _precoBruto * (tabelaPreco.getPercentualPadrao() / 100);
        } else {
            _precoBruto -= _precoBruto * (tabelaPreco.getPercentualPadrao() / 100);
        }

        //Multiplica o preço bruto pelo peso caso o peso seja maior que 0.00
        if (produto.getPesoUnidade() > 0.00) _precoBruto = _precoBruto * produto.getPesoUnidade();

        //Calcula o valor do imposto do produto (%)
        _precoBruto += _precoBruto * (produto.getPorcentagemImposto() / 100);

        _desconto = (100 * _valorDesconto) / _precoBruto;

        txvDesconto.setText(Repositorio.formataNumero(_desconto));
    }

    private void gravar() {
        if (validaCampos().equals("")) {
            pedidoLancto.setIdPedido(pedido.getId());
            pedidoLancto.setIdEstab(pedido.getIdEstab());
            pedidoLancto.setIdProduto(Integer.valueOf(produto.getId()));
            pedidoLancto.setIdEmbalagem(1);

            PedidoLanctoDao pedidoLanctoDao = new PedidoLanctoDao(getApplicationContext());

            pedidoLancto.setSequencia(pedidoLanctoDao.nextSeq(String.valueOf(pedido.getId())));
            pedidoLancto.setQuantidade(Double.valueOf(txvQuantidade.getText().toString().replace(".", "").replace(",", ".")));
            pedidoLancto.setPreco(produto.getPreco());
            pedidoLancto.setDescItem(Double.valueOf(txvDesconto.getText().toString().replace(".", "").replace(",", ".")));
            pedidoLancto.setValorDescontoVisualizacao(Double.valueOf(txvValorDesconto.getText().toString().replace(".", "").replace(",", ".")));

            pedidoLancto.setValorFlexProduto((pedidoLancto.getValorDesconto() * pedidoLancto.getQuantidade()) * -1);

            pedidoLancto.setPorcentagemImposto(produto.getPorcentagemImposto());
            pedidoLancto.setPesoUnidade(produto.getPesoUnidade());

            long _gravar = pedidoLanctoDao.save(pedidoLancto);

            if (_gravar > 0) {
                Toast.makeText(getApplicationContext(), "Produto adicionado com sucesso", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Erro ao adicionar o produto", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), validaCampos(), Toast.LENGTH_LONG).show();
        }
    }

    private String validaCampos() {

        //Obtem o desconto e a quantidade pra validar
        double _qtd = 0.00;
        double _desconto = 0.00;

        _desconto = Double.valueOf(txvDesconto.getText().toString().replace(".", "").replace(",", "."));
        _qtd = Double.valueOf(txvQuantidade.getText().toString().replace(".", "").replace(",", "."));

        if (produto == null) {
            return "Selecione um produto antes de gravar";
        } else if (_qtd < 1) {
            return "Informe uma quantidade válida";
        } else if (!(_desconto < 100)) {
            return "O desconto precisa ser menor que 100%";
        } else if (_desconto > departamento.getDescontoMaximo()) {
            return "Desconto acima do permitido";
        } else {
            return "";
        }

    }

    private Departamento carregaDepartamento() {
        DepartamentoDao _departamentoDao = new DepartamentoDao(PedidoLanctoActivity.this);
        return _departamentoDao.carrega(idDepartamento);
    }

    private Produto carregaProduto() {
        ProdutoDao _produtoDao = new ProdutoDao(PedidoLanctoActivity.this);
        return _produtoDao.retorna(pedidoLancto.getIdProduto(), pedido.getIdEstab());
    }

    private TabelaPreco carregaTabPreco() {
        TabelaPrecoDao _tabelaPrecoDao = new TabelaPrecoDao(PedidoLanctoActivity.this);
        return _tabelaPrecoDao.retorna(pedido.getIdTabelaPreco());
    }
}
