package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.higher.construshow.R;
import com.higher.construshow.db.Database;
import com.higher.construshow.db.VendedorDao;
import com.higher.construshow.model.Empresa;
import com.higher.construshow.model.Vendedor;
import com.higher.construshow.util.Sincronizacao;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrincipalActivity extends AppCompatActivity {

    @BindView(R.id.pedidos)
    Button pedidos;
    @BindView(R.id.clientes)
    Button clientes;
    @BindView(R.id.relatorios)
    Button relatorios;
    @BindView(R.id.historico)
    Button historico;
    @BindView(R.id.importar)
    Button importarDados;
    @BindView(R.id.txvEmpresa)
    TextView txvEmpresa;
    @BindView(R.id.txvVendedor)
    TextView txvVendedor;

    private Vendedor vendedor = null;
    public static Empresa empresa = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        empresa = (Empresa) getIntent().getSerializableExtra("empresa");

        retornaVendedor();

        if(empresa != null){
            if (vendedor != null) {
                txvVendedor.setText(vendedor.getId() + " - " + vendedor.getNome());
                txvEmpresa.setText(empresa.getId() + " - " + empresa.getNome());
            }else{
                Toast.makeText(getApplicationContext(), "Houve um erro ao carregar seus dados, faça login novamente", Toast.LENGTH_LONG).show();
                finish();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Houve um erro ao selecionar a empresa, tente novamente", Toast.LENGTH_LONG).show();
            finish();
        }

        Sincronizacao sincronizacao = new Sincronizacao(PrincipalActivity.this);
        sincronizacao.sincronizaProdutos();

        pedidos.setOnClickListener(view -> startActivity(new Intent(PrincipalActivity.this, PedidosActivity.class)));

        clientes.setOnClickListener(view -> startActivity(new Intent(PrincipalActivity.this, ClientesActivity.class)));

        relatorios.setOnClickListener(view -> startActivity(new Intent(PrincipalActivity.this, RelatoriosView.class)));

        importarDados.setOnClickListener(view -> startActivity(new Intent(PrincipalActivity.this, ImportarActivity.class)));
    }

    private void retornaVendedor() {
        VendedorDao vendedorDao = new VendedorDao(getApplicationContext());

        vendedor = vendedorDao.retorna();
    }
}
