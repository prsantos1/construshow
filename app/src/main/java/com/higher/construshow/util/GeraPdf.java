package com.higher.construshow.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class GeraPdf {
    private static final String PATH = "";
    private Context context;

    private Document document;
    private File pdfFile;
    private PdfWriter pdfWriter;
    private Paragraph paragraph;

    private Font fontTitle = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
    private Font fontSubTitle = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
    private Font fontText = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
    private Font fontNormal = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);
    private Font fontHighText = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);

    public GeraPdf(Context _c){
        this.context = _c;
    }

    public void openDocument(String numPedido){
        createFile(numPedido);

        try{
            document = new Document(PageSize.A4);
            pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
            document.open();

        }catch (Exception e){
            Log.d("openDocument", e.toString());
        }
    }

    public void createFile(String numPedido){
        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "PDF");

        if(!folder.exists())
            folder.mkdirs();

        pdfFile = new File(folder, "orcamento_" + numPedido + ".pdf");

    }

    public void closeDocument(){
        document.close();
    }

    public void addMetaData(String title, String subject, String author){
        document.addTitle(title);
        document.addSubject(subject);
        document.addAuthor(author);
    }

    public void addTitles(String title, String subTitle, String date){

        try{
            paragraph = new Paragraph();
            addChildP(new Paragraph(title, fontTitle));
            addChildP(new Paragraph(subTitle, fontSubTitle));
            addChildP(new Paragraph(date, fontHighText));
            paragraph.setSpacingAfter(30);
            document.add(paragraph);
        }catch (Exception e){
            Log.d("addTitles", e.toString());
        }

    }

    public void addChildP(Paragraph childParagraph){
        childParagraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(childParagraph);
    }

    public void addParagraph(String text){

        try {
            paragraph = new Paragraph(text, fontText);
            paragraph.setSpacingAfter(5);
            paragraph.setSpacingBefore(5);
            document.add(paragraph);
        }catch (Exception e){
            Log.d("add paragraph", e.toString());
        }
    }

    public void addDadosCliente(String cliente, String endereco, String cidade){
        try{
            paragraph = new Paragraph(cliente, fontNormal);
            document.add(paragraph);
            paragraph = new Paragraph(endereco, fontNormal);
            document.add(paragraph);
            paragraph = new Paragraph(cidade, fontNormal);
            paragraph.setSpacingAfter(20);
            document.add(paragraph);
        }catch (Exception e){
            Log.d("add cliente", e.toString());
        }
    }

    public void addTotalizadores(int qtd, String dataPedido, String total){
        try{
            paragraph = new Paragraph("Quantidade de itens: " + qtd, fontText);
            paragraph.setSpacingBefore(40);
            document.add(paragraph);
            paragraph = new Paragraph("Data do pedido: " + dataPedido, fontText);
            document.add(paragraph);
            paragraph = new Paragraph("Valor total do pedido: R$ " + total, fontText);
            document.add(paragraph);
        }catch (Exception e){
            Log.d("add totalizadores", e.toString());
        }
    }

    public void addVendedor(String nome){
        try{
            paragraph = new Paragraph("Vendedor: " + nome, fontText);
            paragraph.setSpacingAfter(10);
            document.add(paragraph);
        }catch (Exception e){
            Log.d("add vendedor", e.toString());
        }
    }

    public void createTable(String[] header, ArrayList<String[]> lista){
        try {
            paragraph = new Paragraph();
            paragraph.setFont(fontText);

            PdfPTable pdfPTable = new PdfPTable(header.length);
            pdfPTable.setWidthPercentage(100);

            PdfPCell pdfPCell;
            int indexC=0;

            while(indexC < header.length){
                pdfPCell= new PdfPCell(new Phrase(header[indexC], fontText));
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setFixedHeight(30);
                pdfPTable.setWidths(new float[]{ 3, 1, 1, 1, 2});

                pdfPTable.addCell(pdfPCell);
                indexC++;
            }

            for(int i=0; i<lista.size();i++){
                String[] row = lista.get(i);

                for(indexC=0; indexC < row.length; indexC++){

                    pdfPCell = new PdfPCell(new Phrase(row[indexC]));
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    pdfPCell.setFixedHeight(40);
                    pdfPTable.addCell(pdfPCell);
                }
            }

            paragraph.add(pdfPTable);

            document.add(paragraph);
        }catch (Exception e){
            Log.d("createTable", e.toString());
        }
    }

    public void appViewPdf(Activity activity){

        if(pdfFile.exists()){
            Uri uri =Uri.fromFile(pdfFile);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "application/pdf");

            try {
                activity.startActivity(intent);

            }catch (ActivityNotFoundException e){
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.adobe.reader")));
                Toast.makeText(activity.getApplicationContext(), "Instale um app para visualizar o pdf", Toast.LENGTH_LONG);
            }
        }else{
            Toast.makeText(activity.getApplicationContext(), "Arquivo não encontrado", Toast.LENGTH_LONG);
        }
    }

    public void pdfWhatsapp(Activity activity){

        if(pdfFile.exists()){
            Uri uri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", pdfFile);

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("application/pdf");
            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
            sendIntent.createChooser(sendIntent, "Selecione o aplicativo");

            activity.startActivity(sendIntent);
        }
    }
}
