package com.higher.construshow.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.higher.construshow.db.PedidoDao;
import com.higher.construshow.db.PedidoLanctoDao;
import com.higher.construshow.model.Pedido;
import com.higher.construshow.model.PedidoLancto;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Repositorio {

    public static String IP_CONEXAO_2 = "http://131.100.251.82:8086/grupo_itamar/public/api/";

    public static String retornaIpConexao() {
        return "http://131.100.251.82:8086/construshow_api/public/api/";
    }

    public static String dataTelaParaBanco(String Data) {
        String Retorno = "";
        try {
            Retorno = Data.substring(6, 10) + "-" + Data.substring(3, 5) + "-" + Data.substring(0, 2);
        } catch (Exception e) {
            //
        }
        return (Retorno);
    }

    public static String dataBancoParaTela(String Data) {
        String Retorno = "";
        try {
            Retorno = Data.substring(8, 10) + "/" + Data.substring(5, 7) + "/" + Data.substring(0, 4);
        } catch (Exception e) {
            //
        }
        return (Retorno);
    }

    public static Date retornaDataAtual() {
        return (Calendar.getInstance().getTime());
    }

    public static String formataDataParaSQL(Date Data) {
        SimpleDateFormat Formatador = new SimpleDateFormat("yyyy-MM-dd");
        return (Formatador.format(Data));
    }

    public static String quotedStr(String texto) {
        return ("'" + texto + "'");
    }

    public static String dataParaTela(Date Data) {
        SimpleDateFormat Formatador = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return (Formatador.format(Data));
    }

    public static String dataBancoParaConstrushow(String Data) {
        String Retorno = "";
        try {
            Retorno = Data.substring(0, 4) + "/" + Data.substring(5, 7) + "/" + Data.substring(8, 10);
        } catch (Exception e) {
            //
        }
        return (Retorno);
    }


    public static String formataCnpjCpf(String Texto) {
        String retorno = "";
        try {
            if (Texto.trim().length() == 14) {
                retorno = Texto.substring(0, 2) + "." + Texto.substring(2, 5) + "." + Texto.substring(5, 8) + "/" + Texto.substring(8, 12) + "-" + Texto.substring(12, 14);
            } else if (Texto.trim().length() == 11) {
                retorno = Texto.substring(0, 3) + "." + Texto.substring(3, 6) + "." + Texto.substring(6, 9) + "-" + Texto.substring(9, 11);
            } else {
                retorno = "";
            }
        } catch (Exception e) {
            //
        }
        return (retorno);
    }

    public static String formataCep(String Texto) {
        String retorno = "";
        try {
            if (Texto.trim().length() == 8) {
                retorno = Texto.substring(0, 2) + "." + Texto.substring(2, 5) + "-" + Texto.substring(5, 8);
            } else {
                retorno = "";
            }
        } catch (Exception e) {
            //
        }
        return (retorno);
    }

    public static String formataNumero(Double numero) {
        DecimalFormat Retorno = new DecimalFormat("###,###,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));

        return (Retorno.format(numero));
    }

    public static String jsonPedido(Context context, String numPedido) {
        String ret = "";

        PedidoDao pedidoDao = new PedidoDao(context);
        Pedido pedido = pedidoDao.retorna(numPedido);

        Gson gson = new Gson();
        ret = gson.toJson(pedido);

        return ret;
    }

    public static void duplicarPedido(Context context, String numPedido) {
        Toast.makeText(context, "Duplicando pedido, aguarde...", Toast.LENGTH_LONG).show();

        PedidoDao pedidoDao = new PedidoDao(context);
        Pedido pedido = pedidoDao.retorna(numPedido);

        pedido.setData(formataDataParaSQL(retornaDataAtual()));
        pedido.setEnviado(0);
        pedido.setId(pedidoDao.nextNumPedido());

        long id = pedidoDao.insere(pedido);

        if (id > 0) {
            //Retorna os lanctos
            PedidoLanctoDao pedidoLanctoDao = new PedidoLanctoDao(context);
            List<PedidoLancto> lanctoList = pedidoLanctoDao.lista(numPedido);

            int _contador = 0;

            for (PedidoLancto lancto : lanctoList) {
                lancto.setIdPedido((int) id);
                lancto.setValorDesconto(lancto.getValorDescontoUnitario());
                _contador++;
                pedidoLanctoDao.insere(lancto);
            }

            Toast.makeText(context, "Pedido duplicado com sucesso, o número do novo pedido é " + id, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Houve um erro ao duplicar o pedido, tente novamente", Toast.LENGTH_LONG).show();
        }
    }


    public static void exibeDialogDuplicar(Context context, String numPedido) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção");
        builder.setMessage("Deseja duplicar esse pedido?");
        builder.setCancelable(false);
        builder.setPositiveButton("Sim", (dialogInterface, i) -> duplicarPedido(context, numPedido));

        builder.setNegativeButton("Não", (dialogInterface, i) -> Log.d("nao", "clicou"));

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
